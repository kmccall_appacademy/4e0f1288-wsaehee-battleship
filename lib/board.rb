class Board
  attr_accessor :grid

  def self.default_grid
    default_grid = Array.new(10){Array.new(10)}
  end

  def initialize(grid = self.class.default_grid)
    @grid = grid
  end

  def count
    @grid.flatten.select do |pos|
      pos != nil
    end.length
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, val)
    row, col = pos
    grid[row][col] = val
  end

  def empty?(pos = nil)
    pos ? self[pos].nil? : count == 0
  end

  def full?
    @grid.flatten.any? {|pos| pos.nil? } ? false : true
  end

  def place_random_ship
    if full?
      raise "Error, You Can't do that."
    end

    rand_pos = [rand(grid.length), rand(grid.length)]

    until empty?(rand_pos)
      rand_pos = [rand(grid.length), rand(grid.length)]
    end

    self[rand_pos] = :s
  end

  def won?
    count == 0 ? true : false
  end
end
